/**
* URL's principal de la base de datos MongoDB (mLab)
*/
const _SECRET = "ultra_secret_word";
var url = "https://api.mlab.com/api/1/databases/myfirstnextgendb/collections/collection_name?apiKey=tg3ecrIo8YzTu_1ZHS5fsEzupTAaWmEB";
var ldap = url.replace("collection_name","ldap");
var clients = url.replace("collection_name","clients");
var persons = url.replace("collection_name","persons");
var accounts = url.replace("collection_name","accounts");
var movements = url.replace("collection_name","movements");


var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var jwt = require('jwt-simple');
var moment = require('moment');
var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(function(req, res, next){
  res.header("Access-Control-Allow-Origin","*");
  res.header("Access-Control-Allow-Headers","Origin, X-Requested-With, Content-Type, Accept");
  next();
});

var requestjson = require('request-json');
var path = require('path');
app.listen(port);


console.log('NextGen NodeJS App RESTful APIs server has started on: ' + port);

app.get('/',
  function(req, res){
    console.log("Hello from GET / NextGen node.js");
    // Considerar esta línea para el primer test
    //res.send('My first \"Hola mundo\" from node.js');
    res.sendFile(path.join(__dirname, 'index.html'));
  }
);

app.post('/authentication/V00/validate',
  function(req, res){

    var query = "&q={%22user%22:%22" + req.body.user + "%22}";

    console.log("/authentication/00");
    var clienteMlab = requestjson.createClient(ldap + query);
    clienteMlab.get('',
      function(err, resM, body){
        if(err){
          console.log(err);
        }else{
          console.log("OK authentication user:" + req.body.user);
          var json;
          body.forEach(function(elemento) {
             json =elemento;
          });
          if(json!=null || json!=undefined){
            if (json.user == req.body.user && json.password == req.body.password){
              res.send({token: createToken(json.clientId, json.user)});
            }else{
              res.status(403).send({message: 'Datos incorrectos'});
            }
          }else{
            res.status(403).send({message: 'Datos incorrectos'});
          }
        }
      });
  }
);

app.post('/authentication/V00',
  function(req, res){

    var next = "";
    var number;
    var queryNext = "&f={%22clientId%22:1}&l=1&s={%22clientId%22:-1}";

    console.log("next()");
    var clienteMlabNext = requestjson.createClient(ldap + queryNext);
    clienteMlabNext.get('',
      function(err, resM, bodyRes){
        if(err){
          console.log(err);
        }else{
          console.log("OK authentication next()");
          var json;
          bodyRes.forEach(function(elemento) {
             json =elemento;
          });
          if(json!=null || json!=undefined){
            number = json.clientId.substring(1,5);
            number++;
            console.log(json.clientId.substring(0,1) + number);
            next = json.clientId.substring(0,1) + number;
          }else{
            res.status(403).send({message: 'Datos incorrectos'});
          }
        }
      });
    var time;
    for(var i=0; i++; i<99999){
      time = time + i;
    }
    console.log("time" + time);
    console.log("/authentication/00/create");
    console.log(req.body);
    var query = "&q={%22user%22:%22" + req.body.user + "%22}";

    console.log("/authentication/00");
    var clienteMlab = requestjson.createClient(ldap + query);
    var clienteMlabCreate = requestjson.createClient(ldap);
    var clienteMlabClient = requestjson.createClient(clients);
    var clienteMlabPerson = requestjson.createClient(persons);

    clienteMlab.get('',
      function(err, resM, body){
        if(err){
          console.log(err);
        }else{
          console.log("OK authentication user already exists: " + req.body.user);
          var json;
          body.forEach(function(elemento) {
             json =elemento;
          });
          if(json!=null || json!=undefined){
            if (json.user == req.body.user){
              res.status(403).send({message: 'Cliente existente.'});
            }else{
              res.status(403).send({message: 'Datos incorrectos'});
            }
          }else{
            var reqLDAP = {};
            var reqClient = {};
            var reqPerson = {};

            reqLDAP.clientId = next;
            reqLDAP.user = req.body.user;
            reqLDAP.password = req.body.password;
            reqLDAP.attemps = 0;

            reqClient.clientId = next;
            reqClient.mobilePhoneNumber = req.body.user;
            reqClient.status = "AC"

            reqPerson.personId = next;
            reqPerson.lastName = req.body.lastName;
            reqPerson.motherLastName = req.body.motherLastName;
            reqPerson.name = req.body.name;
            reqPerson.birthDate = req.body.birthDate;
            reqPerson.email = req.body.email;

            console.log("reqLDAP.clientId" + reqLDAP.clientId);
            clienteMlabCreate.post('', reqLDAP,
              function(err, resM, body){
                if(err){
                  console.log("Creation failed" + err);
                  res.status(409).send({message: 'Creation failed'});

                }else{
                  console.log("OK authentication LDAP USER has been seccessfully created");
                  clienteMlabClient.post('', reqClient,
                    function(err, resM, body){
                      if(err){
                        console.log("Creation failed" + err);
                        res.status(409).send({message: 'Creation failed'});

                      }else{
                        console.log("OK authentication CLIENT has been seccessfully created");

                        clienteMlabPerson.post('', reqPerson,
                          function(err, resM, body){
                            if(err){
                              console.log("Creation failed" + err);
                              res.status(409).send({message: 'Creation failed'});

                            }else{
                              console.log("OK authentication PERSON has been seccessfully created");
                            }
                          });
                      }
                    });


                  res.status(200).send({message: 'User has been seccessfully created'});
                }
              });
          }
        }
      });
  }
);


app.delete('/authentication/V00',
  function(req, res){

    console.log("DELETE /authentication/00");
    console.log(req.body);
    var query = "&q={%22user%22:%22" + req.body.user + "%22}";

    console.log("/authentication/00");
    var clienteMlab = requestjson.createClient(ldap + query);
    var clienteMlabDelete = requestjson.createClient(ldap);
    clienteMlab.get('',
      function(err, resM, body){
        if(err){
          console.log(err);
        }else{
          console.log("OK authentication user already exists: " + req.body.user);
          var json;
          body.forEach(function(elemento) {
             json =elemento;
          });
          if(json!=null || json!=undefined){
            if (json.user == req.body.user && json.password == req.body.password){
              clienteMlabDelete.delete('', req.body,
                function(err, resM, body){
                  if(err){
                    console.log("Delete failed" + err);
                    res.status(409).send({message: 'Delete failed'});
                  }else{
                    console.log("OK authentication user has been seccessfully deleted");
                    res.status(200).send({message: 'User has been seccessfully deleted'});
                  }
                });
            }else{
              res.status(403).send({message: 'Datos incorrectos'});
            }
          }
        }
      });
  }
);

app.put('/authentication/V00',
  function(req, res){
    console.log("PUT /authentication/00");

    isAuth(req, res);

    var query = "&q={%22user%22:%22" + req.user + "%22}";

    var clienteMlab = requestjson.createClient(ldap + query);
    var clienteMlabUpdate = requestjson.createClient(ldap + query);

    var reqUpdate = {};
    reqUpdate.password = req.body.password;
    reqUpdate.user = req.user;
    reqUpdate.clientId = req.id;
    reqUpdate.attemps = 0;

    clienteMlab.get('',
      function(err, resM, body){
        if(err){
          console.log(err);
        }else{
          console.log("OK authentication user already exists: " + req.user);
          var json;
          body.forEach(function(elemento) {
             json =elemento;
          });
          if(json!=null || json!=undefined){
            if (json.user == req.user){
              clienteMlabUpdate.put('', reqUpdate,
                function(err, resM, body){
                  if(err){
                    console.log("Update failed" + err);
                    res.status(409).send({message: 'Update failed'});
                  }else{
                    console.log("OK authentication user has been seccessfully updated");
                    res.status(200).send({message: 'Users password has been seccessfully updated'});
                  }
                });
            }else{
              res.status(403).send({message: 'Datos incorrectos'});
            }
          }
        }
      });
  }
);

app.get('/profile/V00',
  function(req, res){
    console.log("/profile/00");

    isAuth(req,res);

    var query = "&q={%22mobilePhoneNumber%22:%22" + req.user + "%22}";
    var user = "";
    var status = "";
    var name = "";
    var birthDate = "";
    var email = "";

    var clienteMlabClients = requestjson.createClient(clients + query);
    clienteMlabClients.get('',
      function(err, resM, body){
        if(err){
          console.log(err);
        }else{
          console.log("OK clients");
          body.forEach(function(elemento) {
              var time;
              for(var i=0; i++; i<99999){
                time = time + i;
              }
              console.log("time" + time);
             user = elemento.mobilePhoneNumber;
             status = elemento.status;
          });
        }
      });

    query = "&q={%22personId%22:%22" + req.id + "%22}";

    var clienteMlabPersons = requestjson.createClient(persons + query);
    clienteMlabPersons.get('',
      function(err, resM, body){
        if(err){
          console.log(err);
        }else{
          console.log("OK persons" + body.name);
          body.forEach(function(elemento) {
             name = elemento.name + " " + elemento.lastName + " " + elemento.motherLastName;
             birthDate = elemento.birthDate;
             email = elemento.email;
          });

          res.send({
            user: user,
            status: status,
            name: name,
            birthDate: birthDate,
            email: email});
        }
      });
  }
);

app.get('/accounts/V00',
  function(req, res){
    console.log("/accounts/00");

    isAuth(req,res);

    var query = "&q={%22clientId%22:%22" + req.id + "%22}";

    var clienteMlab = requestjson.createClient(accounts + query);
    clienteMlab.get('',
      function(err, resM, body){
        if(err){
          console.log(err);
        }else{
          console.log("OK accounts");
          res.send(body);
        }
      });
  }
);

app.get('/movements/V00',
  function(req, res){
    console.log("queryParams" + req.query.card);
    console.log("/movements/00");

    var card;
    isAuth(req,res);
    if (req.query.card != undefined){
      card = req.query.card;
    }else {
      return res.status(409).send({message:'Parametros requeridos.'});
    }


    var query = "&q={%22card%22:%22" + card
      + "%22, %22customer%22:%22" + req.id + "%22}"
        +"&s={%22timestamp%22:-1}";

    var clienteMlab = requestjson.createClient(movements + query);
    clienteMlab.get('',
      function(err, resM, body){
        if(err){
          console.log(err);
        }else{
          console.log("OK movements");
          res.send(body);
        }
      });
  }
);

function createToken(data, user){
  const  payload = {
    id: data,
    user: user,
    cre: moment().unix(),
    exp: moment().add(10,"minutes").unix()
  }
  var token;
  token = jwt.encode(payload, _SECRET);
  console.log("Ok token");
  return token;
}


function isAuth(req, res){
  console.log("isAuth?");
  if (!req.headers.authorization){
    return res.status(403).send({message:'Unauthorized'});
  }
  const payload = jwt.decode(req.headers.authorization, _SECRET);
  console.log(payload.id +" " + payload.user);
  req.id = payload.id;
  req.user = payload.user;
  console.log("isAuth");

  return;
}
